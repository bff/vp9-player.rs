#![allow(non_camel_case_types)]
#![allow(unused_imports)]

use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::io::Read;
use std::io::Write;
use std::env::args;

use mkv::elements::ElementContent::{Binary, Master};
use mkv::elements::Element;
use mkv::elements::database::{Class, id_to_class};
use mkv::elements::parser::Parser;
use mkv::elements::parser::EventsHandler;

extern crate mkv;
extern crate log;

mod utils;

use utils::four_u8_to_u64;

const BSIZE : usize = 4096;
fn main() {
    let reader : Box<Read> = match args().len() {
        1 => Box::new(std::io::stdin()),
        2 => Box::new(File::open(Path::new(args().nth(1).unwrap().as_str())).expect("Failed to open the file")),
        _ => panic!("Usage: cargo run -- [filename.mkv]")
    };
    let mut f = BufReader::new(reader);

    let mut dom_builder : mkv::elements::builder::Builder = Default::default();
    {
        let mut m = mkv::elements::parser::new();

        loop {
            let mut b = [0; BSIZE];
            match f.read(&mut b) {
                Ok(x) => match x {
                    0 => break,
                    x => m.feed_bytes(b.split_at(x).0, &mut dom_builder),
                },
                Err(e) => { println!("error reading: {}", e); break; },
            }
        }
    }

    for element in dom_builder.captured_elements() {
        print_elements(element, 0);
    }
}

fn print_elements(element: &Element, level: u8) {
    // indent
    for _ in 0..level {
        print!("\t");
    }

    // print name
    println!("{:?}", &parse_element(element));

    // recurse
    match element.content {
        Master(ref children) => {
            for child in children {
                print_elements(child, level + 1);
            }
        },
        _ => (),
    };
}

fn parse_element(element: &Element) -> String {
    match element.class {
        Class::SeekID => {
            if let Binary(ref bytes) = element.content {
                let raw_id = four_u8_to_u64(bytes.as_slice());
                let class = id_to_class(raw_id);
                format!("SeekID for {:?}", &class)
            } else {
                format!("SeekID malformed")
            }
        },
        Class::SeekPosition => { format!("SeekPosition ({:?})", element.content) },
        _ => format!("{:?}", element.class),
    }
}
