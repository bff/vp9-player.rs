#![allow(dead_code)]
use std::convert::Into;

/// Converts four u8 values into a single u64 value. Useful for parsing EBML IDs from raw bytes
pub fn four_u8_to_u64<B: Into<Vec<u8>>>(bytes: B) -> u64 {
    let mut acc : u64 = 0x00000000;
    let four_bytes: Vec<u8> = bytes.into();
    for byte in four_bytes.iter().take(4) {
        let right_most: u64 = From::from(*byte);
        acc = acc.rotate_left(8) + right_most;
    }
    acc
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_list() {
        assert_eq!(0x00000000, four_u8_to_u64(vec![]));
    }

    #[test]
    fn single_value() {
        assert_eq!(0x00000001, four_u8_to_u64(vec![1u8]));
    }

    #[test]
    fn two_values() {
        assert_eq!(0x00000101, four_u8_to_u64(vec![1u8, 1u8]));
    }

    #[test]
    fn four_values() {
        assert_eq!(0x01010101, four_u8_to_u64(vec![1u8, 1u8, 1u8, 1u8]));
    }

    #[test]
    fn five_values() {
        assert_eq!(0x01020304, four_u8_to_u64(vec![1u8, 2u8, 3u8, 4u8, 5u8]));
    }
}
